# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Hospital Management',
    'version' : '1.0',
    'summary': 'Hospital Management System',
    'sequence': -100,
    'description': """ Hospital Management System """,
    'category': 'Productivity',
    'website': 'https://www.odoo.com/page/billing',
    'depends' : ['base','sale','mail'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'wizard/create_appointment_view.xml',
        'wizard/search_appointment_view.xml',
        'views/patient.xml',
        'views/kids.xml',
        'views/patient_gender_view.xml',
        'views/appointment_view.xml',
        'views/sale.xml',

    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
