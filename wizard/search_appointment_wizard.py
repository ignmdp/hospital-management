from odoo import api, fields, models, _


class SearchAppointmentWizard(models.TransientModel):
    _name = "search.appointment.wizard"
    _description = "Search Appointment Wizard"

    patient_id = fields.Many2one('hospital.patient', string="Patient", required = True)
    date_appointment = fields.Date(string='Date')

    def search_appointment_wizard(self):
        action = self.env.ref('om_hospital.action_appointment').read()[0]
        action['domain'] = [('patient_id', '=', self.patient_id.id)]
        # action = self.env["ir.actions.actions"]._for_xml_id("om_hospital.action_appointment")
        # action['domain'] = [('patient_id', '=', self.patient_id.id)]
        return action
    