from odoo import api, fields, models, _


class CreateAppointmentWizard(models.TransientModel):
    _name = "create.appointment.wizard"
    _description = "Create Appointment Wizard"

    patient_id = fields.Many2one('hospital.patient', string="Patient", required = True)
    date_appointment = fields.Date(string='Date')

    def create_appointment_wizard(self):
        vals = {
            'patient_id': self.patient_id.id,
            'date_appointment': self.date_appointment,
            'gender': self.patient_id.gender
        }
        appointment_rec = self.env['hospital.appointment'].create(vals)
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hospital.appointment',
            'res_id': appointment_rec.id,
            'name': _('Appointment'),
            'view_mode': 'form',
        }
        print("vals......", vals)

    def view_appointment_wizard(self):
        action = self.env.ref('om_hospital.action_appointment').read()[0]
        action['domain'] = [('patient_id', '=', self.patient_id.id)]
        # action = self.env["ir.actions.actions"]._for_xml_id("om_hospital.action_appointment")
        # action['domain'] = [('patient_id', '=', self.patient_id.id)]
        return action
