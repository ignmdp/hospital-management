from odoo import api, fields, models, _

class HospitalAppointment(models.Model):
    _name = "hospital.appointment"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Hospital Appointment"

    name = fields.Char(string='Appointment Reference', required=True, copy=False, readonly=True, default=lambda self: _('New'), tracking=True)
    patient_id = fields.Many2one('hospital.patient', string="Patient",tracking=True)
    age = fields.Integer(string='Age', related='patient_id.age', tracking=True)
    note = fields.Text(string='Description')
    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('done', 'Done'), ('cancel', 'Canceled')], default='draft', string="Status", tracking=True)
    date_appointment = fields.Date(string='Date')
    gender = fields.Selection(
        [
            ('other', 'Other'),
            ('male', 'Male'),
            ('female', 'Female')
        ]
        , string='Gender', required=True, default='other', tracking=True)
    note = fields.Text(string='Description')


    def action_confirm(self):
        self.state = 'confirm'

    def action_done(self):
        self.state = 'done'

    def action_draft(self):
        self.state = 'draft'

    def action_cancel(self):
        self.state = 'cancel'

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'New Appointment'
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('hospital.appointment') or _('New')
        res = super(HospitalAppointment, self).create(vals)
        # print("Res ---->", res)
        # print("Vals ---->", vals)
        return res

    @api.onchange('patient_id')
    def onchange_patient_id(self):
        #print ("Onchange Triggered")
        if self.patient_id:
            if self.patient_id.age:
                self.gender = self.patient_id.gender
            else:
                self.gender = ''
